package com.parcellogisticcompany.webapp.Services;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Odesilatel;

import java.sql.SQLException;
import java.util.List;

public class OdesilatelService {

    IDataMapper iDataMapper = DAOFactory.getSqlDAO(DAOType.ODESILATEL);

    public OdesilatelService() {
    }

    public List<Odesilatel> getOdesilatels() throws SQLException {
        return this.iDataMapper.getAll();
    }

    public Odesilatel getOneOdesilatel(int oId) throws SQLException {
        return (Odesilatel)this.iDataMapper.getById(oId).orElse(null);
    }

    public List<Odesilatel> getAllAdresatOdesiltels(int aId) throws SQLException {
        return this.iDataMapper.getAllByDifferentId(DAOType.ADRESAT,aId);
    }

    public List<Odesilatel> getAllZasilkaOdesilatels(int zId) throws SQLException {
        return this.iDataMapper.getAllByDifferentId(DAOType.ZASILKA,zId);
    }

}
