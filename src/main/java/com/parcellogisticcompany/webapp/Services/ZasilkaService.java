package com.parcellogisticcompany.webapp.Services;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Zasilka;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;

import java.sql.SQLException;
import java.util.List;

public class ZasilkaService {
    IDataMapper dataMapper = DAOFactory.getSqlDAO(DAOType.ZASILKA);

    public ZasilkaService() {
    }

    public List<Zasilka> getZasilkas() throws SQLException {
        return dataMapper.getAll();
    }

    public Zasilka getZasilkaById(int zId) throws SQLException {
        return (Zasilka)this.dataMapper.getById(zId).orElse(null);
    }

    public List<Zasilka> getAllAdresatZasilkas(int aId) throws SQLException {
        return dataMapper.getAllByDifferentId(DAOType.ADRESAT,aId);
    }

    public List<Zasilka> getAllOdesilatelZasilkas(int oId) throws SQLException {
        return dataMapper.getAllByDifferentId(DAOType.ODESILATEL,oId);
    }

    public boolean deleteZasilkaById(int zId) throws SQLException {
        return dataMapper.deleteById(zId);
    }

    public boolean updateZasilka(Zasilka zasilka) throws SQLException {
        return this.dataMapper.update(zasilka);
    }

    public boolean saveNewZasilka(ZasilkaMapper zasilkaMapper) throws SQLException {
        return this.dataMapper.save(zasilkaMapper);
    }
}
