package com.parcellogisticcompany.webapp.Services;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Polozka;

import java.sql.SQLException;
import java.util.List;

public class PolozkaService {

    private IDataMapper dataMapper = DAOFactory.getSqlDAO(DAOType.POLOZKA);

    public List<Polozka> getZasilkaPolozkas(int zId) throws SQLException {
        return this.dataMapper.getAllByDifferentId(DAOType.ZASILKA,zId);
    }
}
