package com.parcellogisticcompany.webapp.Services;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Zamestnanec;

import java.sql.SQLException;
import java.util.List;

public class ZamestnanecService {

    private IDataMapper dataMapper = DAOFactory.getSqlDAO(DAOType.ZAMESTNANEC);

    public List<Zamestnanec> getZamestnanecs() throws SQLException {
        return this.dataMapper.getAll();
    }

    public List<Zamestnanec> getPobockaZamestnanecs(int pbid) throws SQLException {
        return this.dataMapper.getAllByDifferentId(DAOType.POBOCKA,pbid);
    }

    public Zamestnanec getZamestnanecBySurname(String surname) throws SQLException {
        return (Zamestnanec) this.dataMapper.getByString(surname).orElse(null);
    }
}
