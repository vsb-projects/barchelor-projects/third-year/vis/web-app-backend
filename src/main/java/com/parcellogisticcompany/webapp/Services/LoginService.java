package com.parcellogisticcompany.webapp.Services;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Login;

import java.sql.SQLException;

public class LoginService {

    public IDataMapper dataMapper = DAOFactory.getXmlDAO(DAOType.LOGIN);

    public Login getLoginCredentials(Login login) throws SQLException {
        Login validatedLogin = (Login) this.dataMapper.getByString(login.getLogin()).orElse(null);
        if(validatedLogin != null && validatedLogin.getPassword().equals(login.getPassword())){
            return validatedLogin;
        }
        return null;
    }

}
