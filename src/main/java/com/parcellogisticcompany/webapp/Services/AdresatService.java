package com.parcellogisticcompany.webapp.Services;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Adresat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AdresatService {

    private IDataMapper dataMapper = DAOFactory.getSqlDAO(DAOType.ADRESAT);

    public List<Adresat> getAdresats() throws SQLException {
      return this.dataMapper.getAll();
    }

    public Adresat getOneAdresat(int aId) throws SQLException {
        return (Adresat)dataMapper.getById(aId).orElse(null);
    }

    public ResultSet getResultSet() throws SQLException {
        return this.dataMapper.getResultSet();
    }

    public List<Adresat> getAllOdesilatelAdresats(int oid) throws SQLException {
        return this.dataMapper.getAllByDifferentId(DAOType.ODESILATEL,oid);
    }
}
