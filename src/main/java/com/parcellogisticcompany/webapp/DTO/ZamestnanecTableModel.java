package com.parcellogisticcompany.webapp.DTO;

import javafx.beans.property.SimpleStringProperty;

public class ZamestnanecTableModel {

    public String getJmeno() {
        return jmeno.get();
    }

    public SimpleStringProperty jmenoProperty() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno.set(jmeno);
    }

    public ZamestnanecTableModel(SimpleStringProperty jmeno) {
        this.jmeno = jmeno;
    }

    public ZamestnanecTableModel(Zamestnanec zamestnanec) {
        this.jmeno = new SimpleStringProperty(zamestnanec.jmeno + " " + zamestnanec.prijmeni);
    }

    public ZamestnanecTableModel() {

    }

    private SimpleStringProperty jmeno;
}
