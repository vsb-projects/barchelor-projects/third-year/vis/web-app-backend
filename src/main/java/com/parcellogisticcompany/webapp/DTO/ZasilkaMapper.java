package com.parcellogisticcompany.webapp.DTO;

import lombok.ToString;

import java.util.Date;

@ToString
public class ZasilkaMapper {

    private int aId;

    private int oId;
    private String oJmeno;
    private String oPrijmeni;
    private String oUlice;
    private String oMesto;
    private int oPsc;
    private String oStat;
    private String oTyp;

    private String typDoruceni;
    private String typZasilky;
    private String typPlatby;
    private String pojisteni;

    private int zbid;
    private int plKusy;
    private String zbNazev;
    private float zbVaha;
    private int zbKat;
    private int zbRok;

    public ZasilkaMapper() {
    }

    public ZasilkaMapper(int aId, int oId, String oJmeno, String oPrijmeni, String oUlice, String oMesto, int oPsc, String oStat, String oTyp, String typDoruceni, String typZasilky, String typPlatby, String pojisteni, int zbid, int plKusy, String zbNazev, float zbVaha, int zbKat, int zbRok) {
        this.aId = aId;
        this.oId = oId;
        this.oJmeno = oJmeno;
        this.oPrijmeni = oPrijmeni;
        this.oUlice = oUlice;
        this.oMesto = oMesto;
        this.oPsc = oPsc;
        this.oStat = oStat;
        this.oTyp = oTyp;
        this.typDoruceni = typDoruceni;
        this.typZasilky = typZasilky;
        this.typPlatby = typPlatby;
        this.pojisteni = pojisteni;
        this.zbid = zbid;
        this.plKusy = plKusy;
        this.zbNazev = zbNazev;
        this.zbVaha = zbVaha;
        this.zbKat = zbKat;
        this.zbRok = zbRok;
    }

    public int getaId() {
        return aId;
    }

    public void setaId(int aId) {
        this.aId = aId;
    }

    public int getoId() {
        return oId;
    }

    public void setoId(int oId) {
        this.oId = oId;
    }

    public String getoJmeno() {
        return oJmeno;
    }

    public void setoJmeno(String oJmeno) {
        this.oJmeno = oJmeno;
    }

    public String getoPrijmeni() {
        return oPrijmeni;
    }

    public void setoPrijmeni(String oPrijmeni) {
        this.oPrijmeni = oPrijmeni;
    }

    public String getoUlice() {
        return oUlice;
    }

    public void setoUlice(String oUlice) {
        this.oUlice = oUlice;
    }

    public String getoMesto() {
        return oMesto;
    }

    public void setoMesto(String oMesto) {
        this.oMesto = oMesto;
    }

    public int getoPsc() {
        return oPsc;
    }

    public void setoPsc(int oPsc) {
        this.oPsc = oPsc;
    }

    public String getoStat() {
        return oStat;
    }

    public void setoStat(String oStat) {
        this.oStat = oStat;
    }

    public String getoTyp() {
        return oTyp;
    }

    public void setoTyp(String oTyp) {
        this.oTyp = oTyp;
    }

    public String getTypDoruceni() {
        return typDoruceni;
    }

    public void setTypDoruceni(String typDoruceni) {
        this.typDoruceni = typDoruceni;
    }

    public String getTypZasilky() {
        return typZasilky;
    }

    public void setTypZasilky(String typZasilky) {
        this.typZasilky = typZasilky;
    }

    public String getTypPlatby() {
        return typPlatby;
    }

    public void setTypPlatby(String typPlatby) {
        this.typPlatby = typPlatby;
    }

    public String getPojisteni() {
        return pojisteni;
    }

    public void setPojisteni(String pojisteni) {
        this.pojisteni = pojisteni;
    }

    public int getZbid() {
        return zbid;
    }

    public void setZbid(int zbid) {
        this.zbid = zbid;
    }

    public int getPlKusy() {
        return plKusy;
    }

    public void setPlKusy(int plKusy) {
        this.plKusy = plKusy;
    }

    public String getZbNazev() {
        return zbNazev;
    }

    public void setZbNazev(String zbNazev) {
        this.zbNazev = zbNazev;
    }

    public float getZbVaha() {
        return zbVaha;
    }

    public void setZbVaha(float zbVaha) {
        this.zbVaha = zbVaha;
    }

    public int getZbKat() {
        return zbKat;
    }

    public void setZbKat(int zbKat) {
        this.zbKat = zbKat;
    }

    public int getZbRok() {
        return zbRok;
    }

    public void setZbRok(int zbRok) {
        this.zbRok = zbRok;
    }
}
