package com.parcellogisticcompany.webapp.DTO;

public class Pobocka {
    private int pbId;
    private String ulice;
    private String mesto;
    private String psc;
    private String stat;
    private int stavVytizeni;

    public Pobocka() {
    }

    public int getPbId() {
        return pbId;
    }

    public void setPbId(int pbId) {
        this.pbId = pbId;
    }

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getPsc() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc = psc;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int getStavVytizeni() {
        return stavVytizeni;
    }

    public void setStavVytizeni(int stavVytizeni) {
        this.stavVytizeni = stavVytizeni;
    }

    public Pobocka(int pbId, String ulice, String mesto, String psc, String stat, int stavVytizeni) {

        this.pbId = pbId;
        this.ulice = ulice;
        this.mesto = mesto;
        this.psc = psc;
        this.stat = stat;
        this.stavVytizeni = stavVytizeni;
    }
}
