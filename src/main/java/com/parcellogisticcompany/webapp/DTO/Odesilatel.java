package com.parcellogisticcompany.webapp.DTO;

public class Odesilatel {
    private int oid;
    private String jmeno;
    private String prijmeni;
    private String ulice;
    private String mesto;
    private String psc;
    private String stat;
    private TypOdesilatele typOdesilatele;

    public Odesilatel(){

    }

    public Odesilatel(int oid, String jmeno, String prijmeni, String ulice, String mesto, String psc, String stat, TypOdesilatele typOdesilatele) {
        this.oid = oid;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.ulice = ulice;
        this.mesto = mesto;
        this.psc = psc;
        this.stat = stat;
        this.typOdesilatele = typOdesilatele;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getPsc() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc = psc;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public TypOdesilatele getTypOdesilatele() {
        return typOdesilatele;
    }

    public void setTypOdesilatele(TypOdesilatele typOdesilatele) {
        this.typOdesilatele = typOdesilatele;
    }
}
