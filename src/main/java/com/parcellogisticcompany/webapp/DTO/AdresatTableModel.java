package com.parcellogisticcompany.webapp.DTO;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AdresatTableModel {

    private SimpleIntegerProperty aid;
    private SimpleStringProperty jmeno;
    private SimpleStringProperty prijmeni;
    private SimpleStringProperty ulice;
    private SimpleStringProperty mesto;
    private SimpleStringProperty psc;
    private SimpleStringProperty stat;
    private SimpleStringProperty typAdresata;

    public AdresatTableModel(SimpleIntegerProperty aid, SimpleStringProperty jmeno, SimpleStringProperty prijmeni, SimpleStringProperty ulice, SimpleStringProperty mesto, SimpleStringProperty psc, SimpleStringProperty stat, SimpleStringProperty typAdresata) {
        this.aid = aid;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.ulice = ulice;
        this.mesto = mesto;
        this.psc = psc;
        this.stat = stat;
        this.typAdresata = typAdresata;
    }

    public AdresatTableModel(int aid, String jmeno, String prijmeni, String ulice, String mesto, String psc, String stat, String typAdresata) {
        this.aid = new SimpleIntegerProperty(aid);
        this.jmeno = new SimpleStringProperty(jmeno);
        this.prijmeni = new SimpleStringProperty(prijmeni);
        this.ulice = new SimpleStringProperty(ulice);
        this.mesto = new SimpleStringProperty(mesto);
        this.psc = new SimpleStringProperty(psc);
        this.stat = new SimpleStringProperty(stat);
        this.typAdresata = new SimpleStringProperty(typAdresata);
    }

    public AdresatTableModel() {

    }

    public Integer getAid() {
        return aid.get();
    }

    public SimpleIntegerProperty aidProperty() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid.set(aid);
    }

    public String getJmeno() {
        return jmeno.get();
    }

    public SimpleStringProperty jmenoProperty() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno.set(jmeno);
    }

    public String getPrijmeni() {
        return prijmeni.get();
    }

    public SimpleStringProperty prijmeniProperty() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni.set(prijmeni);
    }

    public String getUlice() {
        return ulice.get();
    }

    public SimpleStringProperty uliceProperty() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice.set(ulice);
    }

    public String getMesto() {
        return mesto.get();
    }

    public SimpleStringProperty mestoProperty() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto.set(mesto);
    }

    public String getPsc() {
        return psc.get();
    }

    public SimpleStringProperty pscProperty() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc.set(psc);
    }

    public String getStat() {
        return stat.get();
    }

    public SimpleStringProperty statProperty() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat.set(stat);
    }

    public String getTypAdresata() {
        return typAdresata.get();
    }

    public SimpleStringProperty typAdresataProperty() {
        return typAdresata;
    }

    public void setTypAdresata(String typAdresata) {
        this.typAdresata.set(typAdresata);
    }
}
