package com.parcellogisticcompany.webapp.DTO;

public class TypAdresata {
    public String nazev;
    public int cena;

    public TypAdresata(){

    }

    public TypAdresata(String nazev, int cena) {
        this.nazev = nazev;
        this.cena = cena;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }
}
