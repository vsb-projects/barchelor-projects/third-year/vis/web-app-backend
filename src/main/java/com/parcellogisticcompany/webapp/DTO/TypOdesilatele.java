package com.parcellogisticcompany.webapp.DTO;

public class TypOdesilatele {
    String nazev;

    public TypOdesilatele() {
    }

    int cena;

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public TypOdesilatele(String nazev, int cena) {

        this.nazev = nazev;
        this.cena = cena;
    }
}
