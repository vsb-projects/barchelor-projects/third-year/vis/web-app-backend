package com.parcellogisticcompany.webapp.DTO;

public class Polozka {
    public int pid;
    public int kusy;
    public Zasilka zasilka;
    public Zbozi zbozi;

    public Polozka(int pid, int kusy, Zasilka zasilka, Zbozi zbozi) {
        this.pid = pid;
        this.kusy = kusy;
        this.zasilka = zasilka;
        this.zbozi = zbozi;
    }

    public Polozka() {
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getKusy() {
        return kusy;
    }

    public void setKusy(int kusy) {
        this.kusy = kusy;
    }

    public Zasilka getZasilka() {
        return zasilka;
    }

    public void setZasilka(Zasilka zasilka) {
        this.zasilka = zasilka;
    }

    public Zbozi getZbozi() {
        return zbozi;
    }

    public void setZbozi(Zbozi zbozi) {
        this.zbozi = zbozi;
    }
}
