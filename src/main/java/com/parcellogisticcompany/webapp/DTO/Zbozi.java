package com.parcellogisticcompany.webapp.DTO;

public class Zbozi {
    public int zbid;
    public String nazev;
    public double vaha;
    public int kategorie;
    public int rokVyroby;

    public Zbozi(int zbid, String nazev, double vaha, int kategorie, int rokVyroby) {
        this.zbid = zbid;
        this.nazev = nazev;
        this.vaha = vaha;
        this.kategorie = kategorie;
        this.rokVyroby = rokVyroby;
    }

    public Zbozi() {
    }

    public int getZbid() {

        return zbid;
    }

    public void setZbid(int zbid) {
        this.zbid = zbid;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public double getVaha() {
        return vaha;
    }

    public void setVaha(double vaha) {
        this.vaha = vaha;
    }

    public int getKategorie() {
        return kategorie;
    }

    public void setKategorie(int kategorie) {
        this.kategorie = kategorie;
    }

    public int getRokVyroby() {
        return rokVyroby;
    }

    public void setRokVyroby(int rokVyroby) {
        this.rokVyroby = rokVyroby;
    }
}
