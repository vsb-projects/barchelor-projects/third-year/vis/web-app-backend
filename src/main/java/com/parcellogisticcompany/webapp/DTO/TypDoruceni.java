package com.parcellogisticcompany.webapp.DTO;

public class TypDoruceni {

    String nazev;
    int cena;

    public TypDoruceni(String nazev, int cena) {
        this.nazev = nazev;
        this.cena = cena;
    }

    public TypDoruceni() {
    }

    public String getNazev() {

        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }
}
