package com.parcellogisticcompany.webapp.DTO;

import java.util.Date;

public class Zasilka {
    private int zid;
    private Date vytvorena;
    private Date potvrzena;
    private Date dorucena;
    private int cenaZasilky;
    private TypDoruceni typDoruceni;
    private TypZasilky typZasilky;
    private TypPlatby typPlatby;
    private String pojisteni;
    private Adresat adresat;
    private Odesilatel odesilatel;
    private Pobocka pobocka;
    private Zamestnanec zamestnanec;
    //private Polozka polozka;

    public Zasilka() {
    }

    public Zasilka(int zid, Date vytvorena, Date potvrzena, Date dorucena, int cenaZasilky, TypDoruceni typDoruceni, TypZasilky typZasilky, TypPlatby typPlatby, String pojisteni, Adresat adresat, Odesilatel odesilatel, Pobocka pobocka, Zamestnanec zamestnanec) {
        this.zid = zid;
        this.vytvorena = vytvorena;
        this.potvrzena = potvrzena;
        this.dorucena = dorucena;
        this.cenaZasilky = cenaZasilky;
        this.typDoruceni = typDoruceni;
        this.typZasilky = typZasilky;
        this.typPlatby = typPlatby;
        this.pojisteni = pojisteni;
        this.adresat = adresat;
        this.odesilatel = odesilatel;
        this.pobocka = pobocka;
        this.zamestnanec = zamestnanec;
        //this.polozka = polozka;
    }

    public int getZid() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = zid;
    }

    public Date getVytvorena() {
        return vytvorena;
    }

    public void setVytvorena(Date vytvorena) {
        this.vytvorena = vytvorena;
    }

    public Date getPotvrzena() {
        return potvrzena;
    }

    public void setPotvrzena(Date potvrzena) {
        this.potvrzena = potvrzena;
    }

    public Date getDorucena() {
        return dorucena;
    }

    public void setDorucena(Date dorucena) {
        this.dorucena = dorucena;
    }

    public int getCenaZasilky() {
        return cenaZasilky;
    }

    public void setCenaZasilky(int cenaZasilky) {
        this.cenaZasilky = cenaZasilky;
    }

    public TypDoruceni getTypDoruceni() {
        return typDoruceni;
    }

    public void setTypDoruceni(TypDoruceni typDoruceni) {
        this.typDoruceni = typDoruceni;
    }

    public TypZasilky getTypZasilky() {
        return typZasilky;
    }

    public void setTypZasilky(TypZasilky typZasilky) {
        this.typZasilky = typZasilky;
    }

    public TypPlatby getTypPlatby() {
        return typPlatby;
    }

    public void setTypPlatby(TypPlatby typPlatby) {
        this.typPlatby = typPlatby;
    }

    public String getPojisteni() {
        return pojisteni;
    }

    public void setPojisteni(String pojisteni) {
        this.pojisteni = pojisteni;
    }

    public Adresat getAdresat() {
        return adresat;
    }

    public void setAdresat(Adresat adresat) {
        this.adresat = adresat;
    }

    public Odesilatel getOdesilatel() {
        return odesilatel;
    }

    public void setOdesilatel(Odesilatel odesilatel) {
        this.odesilatel = odesilatel;
    }

    public Pobocka getPobocka() {
        return pobocka;
    }

    public void setPobocka(Pobocka pobocka) {
        this.pobocka = pobocka;
    }

    public Zamestnanec getZamestnanec() {
        return zamestnanec;
    }

    public void setZamestnanec(Zamestnanec zamestnanec) {
        this.zamestnanec = zamestnanec;
    }

}
