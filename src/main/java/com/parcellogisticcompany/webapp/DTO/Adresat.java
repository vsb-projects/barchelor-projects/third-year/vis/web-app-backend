package com.parcellogisticcompany.webapp.DTO;

public class Adresat {
    private int aid;
    private String jmeno;
    private String prijmeni;
    private String ulice;
    private String mesto;
    private String psc;
    private String stat;
    private TypAdresata typAdresata;

    public Adresat(){

    }

    public Adresat(int aid, String jmeno, String prijmeni, String ulice, String mesto, String psc, String stat, TypAdresata typAdresata) {
        this.aid = aid;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.ulice = ulice;
        this.mesto = mesto;
        this.psc = psc;
        this.stat = stat;
        this.typAdresata = typAdresata;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getPsc() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc = psc;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public TypAdresata getTypAdresata() {
        return typAdresata;
    }

    public void setTypAdresata(TypAdresata typAdresata) {
        this.typAdresata = typAdresata;
    }
}
