package com.parcellogisticcompany.webapp.DTO;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class OdesilatelTableModel {

    private SimpleIntegerProperty oid;
    private SimpleStringProperty jmeno;
    private SimpleStringProperty prijmeni;
    private SimpleStringProperty ulice;
    private SimpleStringProperty mesto;
    private SimpleStringProperty psc;
    private SimpleStringProperty stat;
    private SimpleStringProperty typOdesilatele;

    public OdesilatelTableModel(SimpleIntegerProperty oid, SimpleStringProperty jmeno, SimpleStringProperty prijmeni, SimpleStringProperty ulice, SimpleStringProperty mesto, SimpleStringProperty psc, SimpleStringProperty stat, SimpleStringProperty typOdesilatele) {
        this.oid = oid;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.ulice = ulice;
        this.mesto = mesto;
        this.psc = psc;
        this.stat = stat;
        this.typOdesilatele = typOdesilatele;
    }

    public OdesilatelTableModel(int aid, String jmeno, String prijmeni, String ulice, String mesto, String psc, String stat, String typOdesilatele) {
        this.oid = new SimpleIntegerProperty(aid);
        this.jmeno = new SimpleStringProperty(jmeno);
        this.prijmeni = new SimpleStringProperty(prijmeni);
        this.ulice = new SimpleStringProperty(ulice);
        this.mesto = new SimpleStringProperty(mesto);
        this.psc = new SimpleStringProperty(psc);
        this.stat = new SimpleStringProperty(stat);
        this.typOdesilatele = new SimpleStringProperty(typOdesilatele);
    }

    public OdesilatelTableModel() {
    }

    public int getOid() {
        return oid.get();
    }

    public SimpleIntegerProperty oidProperty() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid.set(oid);
    }

    public String getJmeno() {
        return jmeno.get();
    }

    public SimpleStringProperty jmenoProperty() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno.set(jmeno);
    }

    public String getPrijmeni() {
        return prijmeni.get();
    }

    public SimpleStringProperty prijmeniProperty() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni.set(prijmeni);
    }

    public String getUlice() {
        return ulice.get();
    }

    public SimpleStringProperty uliceProperty() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice.set(ulice);
    }

    public String getMesto() {
        return mesto.get();
    }

    public SimpleStringProperty mestoProperty() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto.set(mesto);
    }

    public String getPsc() {
        return psc.get();
    }

    public SimpleStringProperty pscProperty() {
        return psc;
    }

    public void setPsc(String psc) {
        this.psc.set(psc);
    }

    public String getStat() {
        return stat.get();
    }

    public SimpleStringProperty statProperty() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat.set(stat);
    }

    public String getTypOdesilatele() {
        return typOdesilatele.get();
    }

    public SimpleStringProperty typOdesilateleProperty() {
        return typOdesilatele;
    }

    public void setTypOdesilatele(String typOdesilatele) {
        this.typOdesilatele.set(typOdesilatele);
    }
}
