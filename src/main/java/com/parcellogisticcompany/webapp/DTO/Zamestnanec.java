package com.parcellogisticcompany.webapp.DTO;

public class Zamestnanec {
    public int zaid;
    public String jmeno;
    public String prijmeni;
    public String pozice;
    public int rokNarozeni;
    public int platovaTrida;

    public Zamestnanec(int zaid, String jmeno, String prijmeni, String pozice, int rokNarozeni, int platovaTrida) {
        this.zaid = zaid;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.pozice = pozice;
        this.rokNarozeni = rokNarozeni;
        this.platovaTrida = platovaTrida;
    }

    public Zamestnanec() {
    }

    public int getZaid() {

        return zaid;
    }

    public void setZaid(int zaid) {
        this.zaid = zaid;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getPozice() {
        return pozice;
    }

    public void setPozice(String pozice) {
        this.pozice = pozice;
    }

    public int getRokNarozeni() {
        return rokNarozeni;
    }

    public void setRokNarozeni(int rokNarozeni) {
        this.rokNarozeni = rokNarozeni;
    }

    public int getPlatovaTrida() {
        return platovaTrida;
    }

    public void setPlatovaTrida(int platovaTrida) {
        this.platovaTrida = platovaTrida;
    }
}
