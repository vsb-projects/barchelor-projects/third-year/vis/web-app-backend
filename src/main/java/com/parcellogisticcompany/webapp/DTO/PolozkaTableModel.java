package com.parcellogisticcompany.webapp.DTO;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class PolozkaTableModel {
    public SimpleIntegerProperty pid;
    public SimpleIntegerProperty kusy;
    public SimpleStringProperty vaha;
    public SimpleIntegerProperty kategorie;
    public SimpleIntegerProperty rok;
    public SimpleStringProperty nazev;


    public PolozkaTableModel() {
    }

    public PolozkaTableModel(SimpleIntegerProperty pid, SimpleIntegerProperty kusy, SimpleStringProperty vaha, SimpleIntegerProperty kategorie, SimpleIntegerProperty rok, SimpleStringProperty nazev) {
        this.pid = pid;
        this.kusy = kusy;
        this.vaha = vaha;
        this.kategorie = kategorie;
        this.rok = rok;
        this.nazev = nazev;
    }

    public PolozkaTableModel(Polozka polozka){
        this.pid = new SimpleIntegerProperty(polozka.pid);
        this.kusy = new SimpleIntegerProperty(polozka.kusy);
        this.vaha = new SimpleStringProperty(String.valueOf(polozka.getZbozi().vaha));
        this.kategorie = new SimpleIntegerProperty(polozka.getZbozi().kategorie);
        this.rok = new SimpleIntegerProperty(polozka.getZbozi().rokVyroby);
        this.nazev = new SimpleStringProperty(polozka.getZbozi().nazev);
    }

    public int getPid() {
        return pid.get();
    }

    public SimpleIntegerProperty pidProperty() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid.set(pid);
    }

    public int getKusy() {
        return kusy.get();
    }

    public SimpleIntegerProperty kusyProperty() {
        return kusy;
    }

    public void setKusy(int kusy) {
        this.kusy.set(kusy);
    }

    public String getVaha() {
        return vaha.get();
    }

    public SimpleStringProperty vahaProperty() {
        return vaha;
    }

    public void setVaha(String vaha) {
        this.vaha.set(vaha);
    }

    public int getKategorie() {
        return kategorie.get();
    }

    public SimpleIntegerProperty kategorieProperty() {
        return kategorie;
    }

    public void setKategorie(int kategorie) {
        this.kategorie.set(kategorie);
    }

    public int getRok() {
        return rok.get();
    }

    public SimpleIntegerProperty rokProperty() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok.set(rok);
    }

    public String getNazev() {
        return nazev.get();
    }

    public SimpleStringProperty nazevProperty() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev.set(nazev);
    }
}
