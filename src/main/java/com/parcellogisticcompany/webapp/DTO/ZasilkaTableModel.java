package com.parcellogisticcompany.webapp.DTO;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

public class ZasilkaTableModel {

    private SimpleIntegerProperty zid;
    private SimpleStringProperty vytvorena;
    private SimpleStringProperty potvrzena;
    private SimpleStringProperty dorucena;
    private SimpleIntegerProperty cenaZasilky;
    private SimpleStringProperty typDoruceni;
    private SimpleStringProperty typZasilky;
    private SimpleStringProperty typPlatby;
    private SimpleStringProperty pojisteni;
    private SimpleIntegerProperty adresat;
    private SimpleIntegerProperty odesilatel;
    private SimpleIntegerProperty pobocka;
    private SimpleIntegerProperty zamestnanec;

    public ZasilkaTableModel() {
    }

    public ZasilkaTableModel(SimpleIntegerProperty zid, SimpleStringProperty vytvorena, SimpleStringProperty potvrzena, SimpleStringProperty dorucena, SimpleIntegerProperty cenaZasilky, SimpleStringProperty typDoruceni, SimpleStringProperty typZasilky, SimpleStringProperty typPlatby, SimpleStringProperty pojisteni, SimpleIntegerProperty adresat, SimpleIntegerProperty odesilatel, SimpleIntegerProperty pobocka, SimpleIntegerProperty zamestnanec) {
        this.zid = zid;
        this.vytvorena = vytvorena;
        this.potvrzena = potvrzena;
        this.dorucena = dorucena;
        this.cenaZasilky = cenaZasilky;
        this.typDoruceni = typDoruceni;
        this.typZasilky = typZasilky;
        this.typPlatby = typPlatby;
        this.pojisteni = pojisteni;
        this.adresat = adresat;
        this.odesilatel = odesilatel;
        this.pobocka = pobocka;
        this.zamestnanec = zamestnanec;
    }

    public ZasilkaTableModel(int zid, Date vytvorena, Date potvrzena, Date dorucena, int cenaZasilky, String typDoruceni, String typZasilky, String typPlatby, String pojisteni, Integer adresat, Integer odesilatel, Integer pobocka, Integer zamestnanec) {
        this.zid = new SimpleIntegerProperty(zid);

        if(vytvorena == null)
            this.vytvorena = new SimpleStringProperty("null");
        else
            this.vytvorena = new SimpleStringProperty(vytvorena.toString());

        if(potvrzena == null)
            this.vytvorena = new SimpleStringProperty("null");
        else
            this.vytvorena = new SimpleStringProperty(potvrzena.toString());

        if(dorucena == null)
            this.dorucena = new SimpleStringProperty("null");
        else
            this.dorucena = new SimpleStringProperty(dorucena.toString());

        this.cenaZasilky = new SimpleIntegerProperty(cenaZasilky);
        this.typDoruceni = new SimpleStringProperty(typDoruceni);
        this.typZasilky = new SimpleStringProperty(typZasilky);
        this.typPlatby = new SimpleStringProperty(typPlatby);
        this.pojisteni = new SimpleStringProperty(pojisteni);
        this.adresat = new SimpleIntegerProperty(adresat);
        this.odesilatel = new SimpleIntegerProperty(odesilatel);
        this.pobocka = new SimpleIntegerProperty(pobocka);
        this.zamestnanec = new SimpleIntegerProperty(zamestnanec);
    }

    public ZasilkaTableModel(int zid, String dorucena, int cenaZasilky, String typDoruceni, String typZasilky, String typPlatby){
        this.zid = new SimpleIntegerProperty(zid);
        this.dorucena = new SimpleStringProperty(dorucena);
        this.cenaZasilky = new SimpleIntegerProperty(cenaZasilky);
        this.typDoruceni = new SimpleStringProperty(typDoruceni);
        this.typZasilky = new SimpleStringProperty(typZasilky);
        this.typPlatby = new SimpleStringProperty(typPlatby);
    }

    public int getZid() {
        return zid.get();
    }

    public SimpleIntegerProperty zidProperty() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = new SimpleIntegerProperty(zid);
    }

    public String getVytvorena() {
        return vytvorena.get();
    }

    public SimpleStringProperty vytvorenaProperty() {
        return vytvorena;
    }

    public void setVytvorena(String vytvorena) {
        this.vytvorena.set(vytvorena);
    }

    public String getPotvrzena() {
        return potvrzena.get();
    }

    public SimpleStringProperty potvrzenaProperty() {
        return potvrzena;
    }

    public void setPotvrzena(String potvrzena) {
        this.potvrzena.set(potvrzena);
    }

    public String getDorucena() {
        return dorucena.get();
    }

    public SimpleStringProperty dorucenaProperty() {
        return dorucena;
    }

    public void setDorucena(Date dorucena) {
        this.dorucena.set(dorucena.toString());
    }

    public int getCenaZasilky() {
        return cenaZasilky.get();
    }

    public SimpleIntegerProperty cenaZasilkyProperty() {
        return cenaZasilky;
    }

    public void setCenaZasilky(int cenaZasilky) {
        this.cenaZasilky.set(cenaZasilky);
    }

    public String getTypDoruceni() {
        return typDoruceni.get();
    }

    public SimpleStringProperty typDoruceniProperty() {
        return typDoruceni;
    }

    public void setTypDoruceni(String typDoruceni) {
        this.typDoruceni.set(typDoruceni);
    }

    public String getTypZasilky() {
        return typZasilky.get();
    }

    public SimpleStringProperty typZasilkyProperty() {
        return typZasilky;
    }

    public void setTypZasilky(String typZasilky) {
        this.typZasilky.set(typZasilky);
    }

    public String getTypPlatby() {
        return typPlatby.get();
    }

    public SimpleStringProperty typPlatbyProperty() {
        return typPlatby;
    }

    public void setTypPlatby(String typPlatby) {
        this.typPlatby.set(typPlatby);
    }

    public String getPojisteni() {
        return pojisteni.get();
    }

    public SimpleStringProperty pojisteniProperty() {
        return pojisteni;
    }

    public void setPojisteni(String pojisteni) {
        this.pojisteni.set(pojisteni);
    }

    public void setDorucena(String dorucena) {
        this.dorucena.set(dorucena);
    }

    public int getAdresat() {
        return adresat.get();
    }

    public SimpleIntegerProperty adresatProperty() {
        return adresat;
    }

    public void setAdresat(int adresat) {
        this.adresat.set(adresat);
    }

    public int getOdesilatel() {
        return odesilatel.get();
    }

    public SimpleIntegerProperty odesilatelProperty() {
        return odesilatel;
    }

    public void setOdesilatel(int odesilatel) {
        this.odesilatel.set(odesilatel);
    }

    public int getPobocka() {
        return pobocka.get();
    }

    public SimpleIntegerProperty pobockaProperty() {
        return pobocka;
    }

    public void setPobocka(int pobocka) {
        this.pobocka.set(pobocka);
    }

    public int getZamestnanec() {
        return zamestnanec.get();
    }

    public SimpleIntegerProperty zamestnanecProperty() {
        return zamestnanec;
    }

    public void setZamestnanec(int zamestnanec) {
        this.zamestnanec.set(zamestnanec);
    }
}
