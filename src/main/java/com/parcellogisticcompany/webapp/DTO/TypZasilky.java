package com.parcellogisticcompany.webapp.DTO;

public class TypZasilky {
    public TypZasilky() {
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    String nazev;
    int cena;

    public TypZasilky(String nazev, int cena) {
        this.nazev = nazev;
        this.cena = cena;
    }
}
