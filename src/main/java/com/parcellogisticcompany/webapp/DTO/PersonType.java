package com.parcellogisticcompany.webapp.DTO;

public enum PersonType {
    Fyzicka_osoba("fyzicka osoba"), Pravnicka_osoba("pravnicka_osoba");

    public String getType() {
        return type;
    }

    final String type;

    PersonType(String type){
        this.type = type;
    }
}
