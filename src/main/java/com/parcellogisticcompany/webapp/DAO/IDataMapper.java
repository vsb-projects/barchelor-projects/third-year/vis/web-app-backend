package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IDataMapper<T> {
    T extract(ResultSet rs, boolean complete) throws SQLException;
    List<T> getAll() throws SQLException;
    List<T> getAllByDifferentId(DAOType daoType, int id) throws SQLException;
    Optional<T> getById(int id) throws SQLException;
    Optional<T> getByString(String str) throws SQLException;
    boolean save(T t) throws SQLException;

    boolean save(ZasilkaMapper zasilka) throws SQLException;

    boolean update(T t) throws SQLException;
    boolean delete(T t) throws SQLException;
    boolean deleteById(int id) throws SQLException;
    ResultSet getResultSet() throws SQLException;

}
