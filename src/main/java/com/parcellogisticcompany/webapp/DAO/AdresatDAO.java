package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.Adresat;
import com.parcellogisticcompany.webapp.DTO.TypAdresata;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;
import com.parcellogisticcompany.webapp.config.NamedParametrStatement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AdresatDAO implements IDataMapper<Adresat>{

    public static Connection connection = null;

    public static String TABLE_NAME = "Adresat";

    public static String SQL_SELECT_ID = "select a.*, ta.cena from adresat a join zasilka z on z.adresat_aid = a.aid and z.adresat_aid = ? join typ_adresata ta on ta.nazev = a.typ_adresata" ;

//    public static String SQL_SELECT_ALL = "select a.* from Adresat a join Zasilka z on z.adresat_aid = a.aid order by aid";

    public static String SQL_SELECT_ALL = "select * from Adresat";

    public static String SQL_SELECT_BY_ODESILATEL = "select a.* from adresat a join zasilka z on z.adresat_aid = a.aid join odesilatel o on z.odesilatel_oid = o.oid and o.oid = ? order by a.aid";

    public static String SQL_SELECT_BY_PARAMS = "select a.* from Adresat a where";

    public static String SQL_UPDATE_ADRESAT = "exec UpdateAdresatAttributes @aid, @ulice, @mesto, @psc, @stat, @typ_adresata";

    public static String SQL_UPDATE = "update Adresat set jmeno = :j, prijmeni = :p, ulice = :u, mesto = :m, psc = :psc, stat = :s, typ_adresata = :ta where aid = :aid" ;

    public static String SQL_INSERT = "insert into Adresat(jmeno,prijmeni,ulice,mesto,psc,stat,typ_adresata) values (?,?,?,?,?,?,?)";

    public static String SQL_DELETE_ID = "delete from historie where adresat_aid = :aid " +
            "delete p from polozka p join zasilka z on z.zid = p.zasilka_zid where z.adresat_aid = :aid " +
            "delete from zasilka where adresat_aid = :aid " +
            "delete from adresat where aid = :aid";



    @Override
    public Adresat extract(ResultSet rs, boolean complete) throws SQLException {
        Adresat adresat = new Adresat();

        adresat.setAid(rs.getInt("aid"));
        adresat.setJmeno(rs.getString("jmeno"));
        adresat.setPrijmeni(rs.getString("prijmeni"));
        adresat.setUlice(rs.getString("ulice"));
        adresat.setMesto(rs.getString("mesto"));
        adresat.setPsc(rs.getString("psc"));
        adresat.setStat(rs.getString("stat"));

        TypAdresata typAdresata = new TypAdresata();

        typAdresata.setNazev(rs.getString("typ_adresata"));

        if(complete)
            typAdresata.setCena(rs.getInt("cena"));

        adresat.setTypAdresata(typAdresata);


        return adresat;
    }

    @Override
    public List<Adresat> getAll() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            List<Adresat> adresats = new ArrayList<Adresat>();

            while(rs.next()){
                adresats.add(extract(rs,false));
            }

            return adresats;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

    @Override
    public List<Adresat> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        switch (daoType){
            case ODESILATEL:
                try{
                    PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_ODESILATEL);
                    preparedStatement.setInt(1,id);

                    ResultSet rs = preparedStatement.executeQuery();

                    List<Adresat> adresats = new ArrayList<Adresat>();

                    while(rs.next()){
                        adresats.add(extract(rs,false));
                    }

                    return adresats;
                } catch (SQLException ex){
                    connection.close();
                    ex.printStackTrace();
                }
            default:
                connection.close();
                return null;
        }
    }

    @Override
    public Optional<Adresat> getById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            Adresat adresat = new Adresat();

            while(rs.next()){
                adresat = extract(rs,true);
            }

            return Optional.of(adresat);
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return Optional.empty();
    }

    @Override
    public Optional<Adresat> getByString(String str) throws SQLException {
        return Optional.empty();
    }

    @Override
    public boolean save(Adresat adresat) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1,adresat.getJmeno());
            preparedStatement.setString(2,adresat.getPrijmeni());
            preparedStatement.setString(3,adresat.getUlice());
            preparedStatement.setString(4,adresat.getMesto());
            preparedStatement.setString(5,adresat.getPsc());
            preparedStatement.setString(6,adresat.getStat());
            preparedStatement.setString(7,adresat.getTypAdresata().getNazev());

            int i = preparedStatement.executeUpdate();

            if(i == 1)
                return true;

        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Adresat adresat) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_UPDATE);

            np.setString("j",adresat.getJmeno());
            np.setString("p",adresat.getPrijmeni());
            np.setString("u",adresat.getUlice());
            np.setString("m",adresat.getMesto());
            np.setString("psc",adresat.getPsc());
            np.setString("s",adresat.getStat());
            np.setString("ta",adresat.getTypAdresata().getNazev());
            np.setInt("aid",adresat.getAid());

            int i = np.executeUpdate();

            if(i == 1)
                return true;

        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean delete(Adresat adresat) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_DELETE_ID);

            np.setInt("aid",adresat.getAid());

            int i = np.executeUpdate();

            if(i == 1)
                return true;
        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            return rs;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

    /*
    private static Adresat extractAdresat(ResultSet rs, boolean complete) throws SQLException {

        Adresat adresat = new Adresat();

        adresat.setAid(rs.getInt("aid"));
        adresat.setJmeno(rs.getString("jmeno"));
        adresat.setPrijmeni(rs.getString("prijmeni"));
        adresat.setUlice(rs.getString("ulice"));
        adresat.setMesto(rs.getString("mesto"));
        adresat.setPsc(rs.getString("psc"));
        adresat.setStat(rs.getString("stat"));

        TypAdresata typAdresata = new TypAdresata();

        typAdresata.setNazev(rs.getString("typ_adresata"));

        if(complete)
            typAdresata.setCena(rs.getInt("cena"));

        adresat.setTypAdresata(typAdresata);


        return adresat;
    }


    public static List<Adresat> getAdresats() throws SQLException {

        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            List<Adresat> adresats = new ArrayList<Adresat>();

            while(rs.next()){
                adresats.add(extractAdresat(rs,false));
            }

            return adresats;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

    public static Adresat getAdresatById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            Adresat adresat = new Adresat();

            while(rs.next()){
                adresat = extractAdresat(rs,true);
            }

            return adresat;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return null;
    }

    public boolean addAdresat(Adresat adresat) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
            preparedStatement.setString(1,adresat.getJmeno());
            preparedStatement.setString(2,adresat.getPrijmeni());
            preparedStatement.setString(3,adresat.getUlice());
            preparedStatement.setString(4,adresat.getMesto());
            preparedStatement.setString(5,adresat.getPsc());
            preparedStatement.setString(6,adresat.getStat());
            preparedStatement.setString(7,adresat.getTypAdresata().getNazev());

            int i = preparedStatement.executeUpdate();

            if(i == 1)
                return true;

        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return false;
    }

    public boolean updateAdresat(Adresat adresat) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_UPDATE);

            np.setString("j",adresat.getJmeno());
            np.setString("p",adresat.getPrijmeni());
            np.setString("u",adresat.getUlice());
            np.setString("m",adresat.getMesto());
            np.setString("psc",adresat.getPsc());
            np.setString("s",adresat.getStat());
            np.setString("ta",adresat.getTypAdresata().getNazev());
            np.setInt("aid",adresat.getAid());

            int i = np.executeUpdate();

            if(i == 1)
                return true;

        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    public boolean deleteAdresat(Adresat adresat) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_DELETE_ID);

            np.setInt("aid",adresat.getAid());

            int i = np.executeUpdate();

            if(i == 1)
                return true;
        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }*/

}
