package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.Polozka;
import com.parcellogisticcompany.webapp.DTO.Zasilka;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.DTO.Zbozi;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PolozkaDAO implements IDataMapper<Polozka> {

    Connection connection = null;

    private IDataMapper zboziDataMapper = DAOFactory.getSqlDAO(DAOType.ZBOZI);

    public final String SQL_SELECT_POLOZKY_ZASILKA = "select p.* from Polozka p where p.zasilka_zid = ?";

    public PolozkaDAO() {
    }


    @Override
    public Polozka extract(ResultSet rs, boolean complete) throws SQLException {
        Polozka polozka = new Polozka();

        polozka.setPid(rs.getInt("pid"));
        polozka.setKusy(rs.getInt("kusy"));

        Zasilka zasilka = new Zasilka();
        zasilka.setZid(rs.getInt("zasilka_zid"));



        Zbozi zbozi = new Zbozi();
        zbozi = (Zbozi) zboziDataMapper.getById(rs.getInt("zbozi_zbid")).orElse(null);

        polozka.setZasilka(zasilka);
        polozka.setZbozi(zbozi);

        return polozka;
    }

    @Override
    public List<Polozka> getAll() throws SQLException {
        return null;
    }

    @Override
    public List<Polozka> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_POLOZKY_ZASILKA);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            List<Polozka> polozkas = new ArrayList<>();

            while(rs.next()){
                polozkas.add(extract(rs,false));
            }

            return polozkas;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return null;
    }

    @Override
    public Optional<Polozka> getById(int id) throws SQLException {
        return Optional.empty();
    }

    @Override
    public Optional<Polozka> getByString(String str) throws SQLException {
        return Optional.empty();
    }

    @Override
    public boolean save(Polozka polozka) throws SQLException {
        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Polozka polozka) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Polozka polozka) throws SQLException {
        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return null;
    }
}
