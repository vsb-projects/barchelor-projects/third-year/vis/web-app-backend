package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.Odesilatel;
import com.parcellogisticcompany.webapp.DTO.TypOdesilatele;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;
import com.parcellogisticcompany.webapp.config.NamedParametrStatement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OdesilatelDAO implements IDataMapper<Odesilatel> {

    Connection connection = null;

    public static String TABLE_NAME = "Odesilatel";

    public static String SQL_SELECT_ID = "select o.*, tod.cena from odesilatel o join zasilka z on z.odesilatel_oid = o.oid and z.odesilatel_oid = ? join typ_odesilatele tod on tod.nazev = o.typ_odesilatele";

    public static String SQL_SELECT_ALL = "select o.* from odesilatel o join zasilka z on z.odesilatel_oid = o.oid order by oid";

    public static String SQL_SELECT_BY_ADRESAT = "select o.* from odesilatel o join zasilka z on z.odesilatel_oid = o.oid join adresat a on a.aid = z.adresat_aid and a.aid = ? order by o.oid";

    public static String SQL_SELECT_BY_ZASILKA = "select o.* from odesilatel o join zasilka z on z.odesilatel_oid = o.oid and z.zid = ? order by o.oid";

    public static String SQL_SELECT_BY_PARAMS = "select * from Odesilatel where ";

    public static String SQL_UPDATE_ODESILATEL = "exec UpdateOdesilatelAttributes @oid, @ulice, @mesto, @psc, @stat, @typ_odesilatele";

    public static String SQL_ODESILATEL_ZASILKA_ID = "select z.* from Odesilatel o join Zasilka z on o.oid = z.Odesilatel_oid and o.oid = @oid";

    public static String SQL_UPDATE = "update Odesilatel set jmeno = @jmeno, prijmeni = @prijmeni, ulice = @ulice, mesto = @mesto, psc = @psc, stat = @stat, typ_odesilatele = @typ_odesilatele where oid = @oid";

    public static String SQL_INSERT = "insert into Odesilatel(jmeno,prijmeni,ulice,mesto,psc,stat,typ_odesilatele) values (@jmeno,@prijmeni,@ulice,@mesto,@psc,@stat,@typ_odesilatele)";

    public static String SQL_DELETE_ID = "delete p from polozka p join zasilka z on z.zid = p.zasilka_zid where z.odesilatel_oid = @oid " +
            "delete from zasilka where odesilatel_oid = @oid " +
            "delete from odesilatel where oid = @oid";


    public OdesilatelDAO() {
    }

    @Override
    public Odesilatel extract(ResultSet rs, boolean complete) throws SQLException {
        Odesilatel odesilatel = new Odesilatel();

        odesilatel.setOid(rs.getInt("oid"));
        odesilatel.setJmeno(rs.getString("jmeno"));
        odesilatel.setPrijmeni(rs.getString("prijmeni"));
        odesilatel.setUlice(rs.getString("ulice"));
        odesilatel.setMesto(rs.getString("mesto"));
        odesilatel.setPsc(rs.getString("psc"));
        odesilatel.setStat(rs.getString("stat"));

        TypOdesilatele typOdesilatele = new TypOdesilatele();

        typOdesilatele.setNazev(rs.getString("typ_odesilatele"));

        if(complete)
            typOdesilatele.setCena(rs.getInt("cena"));

        odesilatel.setTypOdesilatele(typOdesilatele);


        return odesilatel;
    }

    @Override
    public List<Odesilatel> getAll() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            List<Odesilatel> odesilatels = new ArrayList<>();

            while(rs.next()){
                odesilatels.add(extract(rs,false));
            }

            return odesilatels;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

    @Override
    public List<Odesilatel> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        switch (daoType){
            case ADRESAT:
                try{
                    PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_ADRESAT);
                    preparedStatement.setInt(1,id);

                    ResultSet rs = preparedStatement.executeQuery();

                    List<Odesilatel> odesilatels = new ArrayList<>();

                    while(rs.next()){
                        odesilatels.add(extract(rs,false));
                    }

                    return odesilatels;
                } catch (SQLException ex){
                    connection.close();
                    ex.printStackTrace();
                }
            case ZASILKA:
                try{
                    PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_ZASILKA);
                    preparedStatement.setInt(1,id);

                    ResultSet rs = preparedStatement.executeQuery();

                    List<Odesilatel> odesilatels = new ArrayList<>();

                    while(rs.next()){
                        odesilatels.add(extract(rs,false));
                    }

                    return odesilatels;
                } catch (SQLException ex){
                    connection.close();
                    ex.printStackTrace();
                }
                default:
                    connection.close();
                    return null;
        }
    }

    @Override
    public Optional<Odesilatel> getById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            Odesilatel odesilatel = new Odesilatel();

            while(rs.next()){
                odesilatel = extract(rs,true);
            }

            return Optional.ofNullable(odesilatel);
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return Optional.empty();
    }

    @Override
    public Optional<Odesilatel> getByString(String str) throws SQLException {
        return Optional.empty();
    }

    @Override
    public boolean save(Odesilatel odesilatel) throws SQLException {
        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Odesilatel odesilatel) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Odesilatel odesilatel) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_DELETE_ID);

            np.setInt("aid",odesilatel.getOid());

            int i = np.executeUpdate();

            if(i == 1)
                return true;
        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            return rs;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }
}
