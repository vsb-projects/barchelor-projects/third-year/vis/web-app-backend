package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.Zamestnanec;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ZamestnanecDAO implements IDataMapper<Zamestnanec> {

    Connection connection = null;

    public static String SQL_SELECT_ALL = "select * from Zamestnanec";

    public static String SQL_SELECT_BY_SURNAME = "select z.* from Zamestnanec z where z.prijmeni = ?";

    public static String SQL_SELECT_ZAMESTNANEC_POBOCKA = "select z.* from Zamestnanec z join pracuje pr on z.zaid = pr.zamestnanec_zaid join Pobocka p on pr.pobocka_pbid = p.pbid and p.pbid = ?";

    @Override
    public Zamestnanec extract(ResultSet rs, boolean complete) throws SQLException {
        Zamestnanec zamestnanec = new Zamestnanec();

        zamestnanec.setZaid(rs.getInt("zaid"));
        zamestnanec.setJmeno(rs.getString("jmeno"));
        zamestnanec.setPlatovaTrida(rs.getInt("platova_trida"));
        zamestnanec.setPrijmeni(rs.getString("prijmeni"));
        zamestnanec.setPozice(rs.getString("pozice"));
        zamestnanec.setRokNarozeni(rs.getInt("rok_narozeni"));

        return zamestnanec;
    }

    @Override
    public List<Zamestnanec> getAll() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            List<Zamestnanec> zamestnanecs = new ArrayList<>();

            while(rs.next()){
                zamestnanecs.add(extract(rs,false));
            }

            return zamestnanecs;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

    @Override
    public List<Zamestnanec> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ZAMESTNANEC_POBOCKA);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            List<Zamestnanec> zamestnanecs = new ArrayList<>();

            while(rs.next()){
                zamestnanecs.add(extract(rs,false));
            }

            return zamestnanecs;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public Optional<Zamestnanec> getById(int id) throws SQLException {
        return Optional.empty();
    }

    @Override
    public Optional<Zamestnanec> getByString(String str) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_SURNAME);
            preparedStatement.setString(1,str);

            ResultSet rs = preparedStatement.executeQuery();

            Zamestnanec zamestnanec = new Zamestnanec();

            while(rs.next()){
                zamestnanec = extract(rs,true);
            }

            return Optional.of(zamestnanec);
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return Optional.empty();
    }

    @Override
    public boolean save(Zamestnanec zamestnanec) throws SQLException {
        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Zamestnanec zamestnanec) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Zamestnanec zamestnanec) throws SQLException {
        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return null;
    }
}
