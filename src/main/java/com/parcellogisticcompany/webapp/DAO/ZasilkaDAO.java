package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.*;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;
import com.parcellogisticcompany.webapp.config.NamedParametrStatement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ZasilkaDAO implements IDataMapper<Zasilka> {

    Connection connection = null;

    private IDataMapper pobockaDataMapper = DAOFactory.getSqlDAO(DAOType.POBOCKA);

    public static String SQL_SELECT_ID = "select z.* from zasilka z where z.zid = ?";

    public static String SQL_SELECT_ALL = "select * from zasilka order by zid";

    public static String SQL_SELECT_ZASILKY_ODESILATEL = "select z.* from Odesilatel o join Zasilka z on o.oID = z.odesilatel_oid and o.oid = ?";

    public static String SQL_SELECT_ZASILKY_ADRESAT = "select z.* from Adresat a join Zasilka z on a.aID = z.adresat_aid and a.aid = ?";

    public static String SQL_UPDATE = "update zasilka set vytvorena = ?, potvrzena = ?, dorucena = ?, cena_zasilky = ?, typ_doruceni = ?, typ_zasilky = ?, typ_platby = ?, pojisteni = ?, adresat_aid = ?, odesilatel_oid = ?, pobocka_pbid = ?, zamestnanec_zaid = ? where zid = ?";

    public static String SQL_UPDATE_ZASILKA = "exec UpdateZasilkaAttributes @zid, @pojisteni, @typ_doruceni, @typ_zasilky, @typ_platby";

    public static String SQL_UPDATE_ZASILKA_ITEMS = "exec AddOrUpdateItemsOfZasilka @zid, @plid, @zbid, @plKusy";

    public static String SQL_ADD_NEW_ZASILKA = "exec AddNewZasilka ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";

    public static String SQL_INSERT = "insert into Zasilka(vytvorena, potvrzena, dorucena, cena_zasilky, typ_doruceni, typ_zasilky, typ_platby, pojisteni, adresat_aid, odesilatel_oid, pobocka_depo_depoid, zamestnanec_zaid) values" +
            " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    public static String SQL_DELETE_ID = "delete p from polozka p join zasilka z on z.zid = p.zasilka_zid where z.zid = :zid1 " +
            "delete from zasilka where zid = :zid2";

    public static String SQL_UPDATE_ZASILKA_POLOZKY = "exec AddOrUpdateItemsOfZasilka @pzid, @plid, @zbid, @kusy";


    public ZasilkaDAO(){

    }

    @Override
    public Zasilka extract(ResultSet rs, boolean complete) throws SQLException {
        Zasilka zasilka = new Zasilka();

        zasilka.setZid(rs.getInt("zid"));
        zasilka.setVytvorena(rs.getDate("vytvorena"));
        zasilka.setPotvrzena(rs.getDate("potvrzena"));
        zasilka.setDorucena(rs.getDate("potvrzena"));
        zasilka.setCenaZasilky(rs.getInt("cena_zasilky"));
        zasilka.setPojisteni(rs.getString("pojisteni"));

        TypDoruceni typDoruceni = new TypDoruceni();
        typDoruceni.setNazev(rs.getString("typ_doruceni"));
        zasilka.setTypDoruceni(typDoruceni);
        TypZasilky typZasilky = new TypZasilky();
        typZasilky.setNazev(rs.getString("typ_zasilky"));
        zasilka.setTypZasilky(typZasilky);
        TypPlatby typPlatby = new TypPlatby();
        typPlatby.setNazev(rs.getString("typ_platby"));
        zasilka.setTypPlatby(typPlatby);

        Adresat adresat = new Adresat();
        adresat.setAid(rs.getInt("adresat_aid"));

        Odesilatel odesilatel = new Odesilatel();
        odesilatel.setOid(rs.getInt("odesilatel_oid"));

        Zamestnanec zamestnanec = new Zamestnanec();
        zamestnanec.setZaid(rs.getInt("zamestnanec_zaid"));

        Pobocka pobocka = new Pobocka();
        pobocka = (Pobocka) pobockaDataMapper.getById(rs.getInt("pobocka_pbid")).orElse(null);

        //Polozka polozka = new Polozka();
        //polozka.setPid(rs.getInt("polozka_pid"));

        zasilka.setAdresat(adresat);
        zasilka.setOdesilatel(odesilatel);
        zasilka.setZamestnanec(zamestnanec);
        zasilka.setPobocka(pobocka);
        //zasilka.setPolozka(polozka);

        return zasilka;
    }

    @Override
    public List<Zasilka> getAll() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            List<Zasilka> zasilkas = new ArrayList<>();

            while(rs.next()){
                zasilkas.add(extract(rs,false));
            }

            return zasilkas;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

    @Override
    public List<Zasilka> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        switch(daoType){
            case ADRESAT:
                try{
                    PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ZASILKY_ADRESAT);
                    preparedStatement.setInt(1,id);

                    ResultSet rs = preparedStatement.executeQuery();

                    List<Zasilka> zasilkas = new ArrayList<>();

                    while(rs.next()){
                        zasilkas.add(extract(rs,false));
                    }

                    return zasilkas;
                } catch (SQLException ex){
                    connection.close();
                    ex.printStackTrace();
                }
            case ODESILATEL:
                try{
                    PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ZASILKY_ODESILATEL);
                    preparedStatement.setInt(1,id);

                    ResultSet rs = preparedStatement.executeQuery();

                    List<Zasilka> zasilkas = new ArrayList<>();

                    while(rs.next()){
                        zasilkas.add(extract(rs,false));
                    }

                    return zasilkas;
                } catch (SQLException ex){
                    connection.close();
                    ex.printStackTrace();
                }
                default:
                    connection.close();
                    return null;
        }




    }

    @Override
    public Optional<Zasilka> getById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            Zasilka zasilka = new Zasilka();

            while(rs.next()){
                zasilka = extract(rs,true);
            }

            return Optional.of(zasilka);
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return Optional.empty();
    }

    @Override
    public Optional<Zasilka> getByString(String str) throws SQLException {
        return Optional.empty();
    }

    @Override
    public boolean save(Zasilka zasilka) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
//            preparedStatement.setDate(1,);


            int i = preparedStatement.executeUpdate();

            if(i == 1)
                return true;

        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return false;
    }


//  ? ,@oid,@oJmeno,@oPrijmeni,@oUlice,@oPsc,@oMesto,@oStat,@oTyp,@pojisteni,@typ_doruceni, @typ_zasilky,@typ_platby,@zbid,@plKusy,@zbNazev,@zbVaha,@zbKat,@zbRok


    @Override
    public boolean save(ZasilkaMapper zasilkaMapper) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEW_ZASILKA);
            preparedStatement.setInt(1,zasilkaMapper.getaId());
            preparedStatement.setInt(2,zasilkaMapper.getoId());
            preparedStatement.setString(3,zasilkaMapper.getoJmeno());
            preparedStatement.setString(4,zasilkaMapper.getoPrijmeni());
            preparedStatement.setString(5,zasilkaMapper.getoUlice());
            preparedStatement.setInt(6,zasilkaMapper.getoPsc());
            preparedStatement.setString(7,zasilkaMapper.getoMesto());
            preparedStatement.setString(8,zasilkaMapper.getoStat());
            preparedStatement.setString(9,zasilkaMapper.getoTyp());
            preparedStatement.setString(10,zasilkaMapper.getPojisteni());
            preparedStatement.setString(11,zasilkaMapper.getTypDoruceni());
            preparedStatement.setString(12,zasilkaMapper.getTypZasilky());
            preparedStatement.setString(13,zasilkaMapper.getTypPlatby());
            preparedStatement.setInt(14,zasilkaMapper.getZbid());
            preparedStatement.setInt(15,zasilkaMapper.getPlKusy());
            preparedStatement.setString(16,zasilkaMapper.getZbNazev());
            preparedStatement.setFloat(17,zasilkaMapper.getZbVaha());
            preparedStatement.setInt(18,zasilkaMapper.getZbKat());
            preparedStatement.setInt(19,zasilkaMapper.getZbRok());

            int i = preparedStatement.executeUpdate();

            if(i == 1)
                return true;

        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return false;
    }

    @Override
    public boolean update(Zasilka zasilka) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        int i = 0;

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE);
            preparedStatement.setDate(++i,new Date(zasilka.getVytvorena().getTime()));
            preparedStatement.setDate(++i,new Date(zasilka.getPotvrzena().getTime()));
            preparedStatement.setDate(++i,new Date(zasilka.getDorucena().getTime()));
            preparedStatement.setInt(++i,zasilka.getCenaZasilky());
            preparedStatement.setString(++i,zasilka.getTypDoruceni().getNazev());
            preparedStatement.setString(++i,zasilka.getTypZasilky().getNazev());
            preparedStatement.setString(++i,zasilka.getTypPlatby().getNazev());
            preparedStatement.setString(++i,zasilka.getPojisteni());
            preparedStatement.setInt(++i,zasilka.getAdresat().getAid());
            preparedStatement.setInt(++i,zasilka.getOdesilatel().getOid());
            preparedStatement.setInt(++i,zasilka.getPobocka().getPbId());
            preparedStatement.setInt(++i,zasilka.getZamestnanec().getZaid());
            preparedStatement.setInt(++i,zasilka.getZid());

            int r = preparedStatement.executeUpdate();

            if(r == 1)
                return true;
        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean delete(Zasilka zasilka) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_DELETE_ID);

            np.setInt("zid",zasilka.getZid());

            int i = np.executeUpdate();

            if(i == 1)
                return true;
        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            NamedParametrStatement np = new NamedParametrStatement(connection,SQL_DELETE_ID);

            np.setInt("zid1",id);
            np.setInt("zid2",id);

            int i = np.executeUpdate();

            if(i == 1)
                return true;
        } catch (SQLException ex){
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQL_SELECT_ALL);

            return rs;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();

        return null;
    }

}
