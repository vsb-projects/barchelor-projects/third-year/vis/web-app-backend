package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.Login;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public class LoginDAO implements IDataMapper<Login> {
    @Override
    public Login extract(ResultSet rs, boolean complete) throws SQLException {
        return null;
    }

    @Override
    public List<Login> getAll() throws SQLException {
        return null;
    }

    @Override
    public List<Login> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        return null;
    }

    @Override
    public Optional<Login> getById(int id) throws SQLException {
        Login login = new Login();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse("loginCredentials.xml");

            // Create XPathFactory object
            XPathFactory xpathFactory = XPathFactory.newInstance();

            // Create XPath object
            XPath xpath = xpathFactory.newXPath();

            login = getUserNameById(doc, xpath, id);
            if(login != null) {
                System.out.println("Employee Name with ID " + id + ": " + login);
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return Optional.ofNullable(login);
    }

    @Override
    public Optional<Login> getByString(String str) throws SQLException {
        Login login = new Login();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse("loginCredentials.xml");

            // Create XPathFactory object
            XPathFactory xpathFactory = XPathFactory.newInstance();

            // Create XPath object
            XPath xpath = xpathFactory.newXPath();

            login = getUserNameByLogin(doc, xpath, str.toLowerCase());
            if(login != null) {
                System.out.println("Employee Name with Login " + str + ": " + login);
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return Optional.ofNullable(login);
    }

    @Override
    public boolean save(Login login) throws SQLException {
        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Login login) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Login login) throws SQLException {
        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return null;
    }

    private Login getUserNameById(Document doc, XPath xpath, int id) {
        Login login = new Login();
        XPathExpression expr;
        try {
            expr = xpath.compile("/Users/User[@id='" + id + "']");

            if(!(Boolean) expr.evaluate(doc,XPathConstants.BOOLEAN)){
                login = null;
                System.out.println("Login not found!");
                return login;
            }
            login.setId(id);
            expr = xpath.compile("/Users/User[@id='" + id + "']/login/text()");
            login.setLogin((String) expr.evaluate(doc, XPathConstants.STRING));
            expr = xpath.compile("/Users/User[@id='" + id + "']/name/text()");
            login.setName((String) expr.evaluate(doc, XPathConstants.STRING));
            expr = xpath.compile("/Users/User[@id='" + id + "']/surname/text()");
            login.setSurname((String) expr.evaluate(doc, XPathConstants.STRING));
            expr = xpath.compile("/Users/User[@id='" + id + "']/password/text()");
            login.setPassword((String) expr.evaluate(doc, XPathConstants.STRING));
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return login;
    }

    private Login getUserNameByLogin(Document doc, XPath xpath, String l) {
        Login login = new Login();
        XPathExpression expr;
        try {
            expr = xpath.compile("/Users/User[login='" + l +"']");

            if(!(Boolean) expr.evaluate(doc,XPathConstants.BOOLEAN)){
                login = null;
                System.out.println("Login not found!");
                return login;
            }
            expr = xpath.compile("/Users/User[login='" + l + "']/@id");
            login.setId(Integer.valueOf((String) expr.evaluate(doc, XPathConstants.STRING)));
            expr = xpath.compile("/Users/User[login='" + l + "']/login/text()");
            login.setLogin((String) expr.evaluate(doc, XPathConstants.STRING));
            expr = xpath.compile("/Users/User[login='" + l + "']/name/text()");
            login.setName((String) expr.evaluate(doc, XPathConstants.STRING));
            expr = xpath.compile("/Users/User[login='" + l + "']/surname/text()");
            login.setSurname((String) expr.evaluate(doc, XPathConstants.STRING));
            expr = xpath.compile("/Users/User[login='" + l + "']/password/text()");
            login.setPassword((String) expr.evaluate(doc, XPathConstants.STRING));
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return login;
    }
}
