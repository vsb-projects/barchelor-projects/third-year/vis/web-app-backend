package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.Pobocka;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PobockaDAO implements IDataMapper<Pobocka> {

    Connection connection = null;

    public final String SQL_SELECT_POBOCKA_ZASILKA = "select p.* from Pobocka p join Zasilka z on z.pobocka_pbid = p.pbid where z.pobocka_pbid = ?";

    public static String SQL_SELECT_ID = "select p.* from Pobocka p where p.pbid = ?";

    @Override
    public Pobocka extract(ResultSet rs, boolean complete) throws SQLException {
        Pobocka pobocka = new Pobocka();

        pobocka.setPbId(rs.getInt("pbid"));
        pobocka.setMesto(rs.getString("mesto"));
        pobocka.setUlice(rs.getString("ulice"));
        pobocka.setPsc(rs.getString("psc"));
        pobocka.setStat(rs.getString("stat"));
        pobocka.setStavVytizeni(rs.getInt("stav_vytizeni"));

        return pobocka;
    }

    @Override
    public List<Pobocka> getAll() throws SQLException {
        return null;
    }

    @Override
    public List<Pobocka> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_POBOCKA_ZASILKA);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            List<Pobocka> pobockas = new ArrayList<>();

            while(rs.next()){
                pobockas.add(extract(rs,false));
            }

            return pobockas;
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return null;
    }

    @Override
    public Optional<Pobocka> getById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            Pobocka pobocka = new Pobocka();

            while(rs.next()){
                pobocka = extract(rs,true);
            }

            return Optional.of(pobocka);
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return Optional.empty();
    }

    @Override
    public Optional<Pobocka> getByString(String str) throws SQLException {
        return Optional.empty();
    }

    @Override
    public boolean save(Pobocka pobocka) throws SQLException {
        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Pobocka pobocka) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Pobocka pobocka) throws SQLException {
        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return null;
    }
}
