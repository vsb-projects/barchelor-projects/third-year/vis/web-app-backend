package com.parcellogisticcompany.webapp.DAO;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.DTO.Zbozi;
import com.parcellogisticcompany.webapp.config.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ZboziDAO implements IDataMapper<Zbozi> {

    Connection connection = null;

    public static String SQL_SELECT_ID = "select z.* from zbozi z where z.zbid = ?";

    @Override
    public Zbozi extract(ResultSet rs, boolean complete) throws SQLException {
        Zbozi zbozi = new Zbozi();

        zbozi.setZbid(rs.getInt("zbid"));
        zbozi.setNazev(rs.getString("nazev"));
        zbozi.setKategorie(rs.getInt("kategorie"));
        zbozi.setRokVyroby(rs.getInt("rok_vyroby"));
        zbozi.setVaha(rs.getDouble("vaha"));

        return zbozi;
    }

    @Override
    public List<Zbozi> getAll() throws SQLException {
        return null;
    }

    @Override
    public List<Zbozi> getAllByDifferentId(DAOType daoType, int id) throws SQLException {
        return null;
    }

    @Override
    public Optional<Zbozi> getById(int id) throws SQLException {
        connection = DatabaseConnection.getDBConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ID);
            preparedStatement.setInt(1,id);

            ResultSet rs = preparedStatement.executeQuery();

            Zbozi zbozi = new Zbozi();

            while(rs.next()){
                zbozi = extract(rs,true);
            }

            return Optional.of(zbozi);
        } catch (SQLException ex){
            connection.close();
            ex.printStackTrace();
        }

        connection.close();
        return Optional.empty();
    }

    @Override
    public Optional<Zbozi> getByString(String str) throws SQLException {
        return Optional.empty();
    }

    @Override
    public boolean save(Zbozi zbozi) throws SQLException {
        return false;
    }

    @Override
    public boolean save(ZasilkaMapper zasilka) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Zbozi zbozi) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Zbozi zbozi) throws SQLException {
        return false;
    }

    @Override
    public boolean deleteById(int id) throws SQLException {
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        return null;
    }
}
