package com.parcellogisticcompany.webapp.DAO.Factory;

public enum DAOType {

    ADRESAT, ODESILATEL, ZASILKA, ZAMESTNANEC, POBOCKA, POLOZKA, ZBOZI, LOGIN;
}
