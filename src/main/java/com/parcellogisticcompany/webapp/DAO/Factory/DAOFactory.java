package com.parcellogisticcompany.webapp.DAO.Factory;

import com.parcellogisticcompany.webapp.DAO.*;

public class DAOFactory {

    public final static IDataMapper getSqlDAO(DAOType daoType){
        switch(daoType){
            case ADRESAT:
                return new AdresatDAO();
            case ODESILATEL:
                return new OdesilatelDAO();
            case ZASILKA:
                return new ZasilkaDAO();
            case POLOZKA:
                return new PolozkaDAO();
            case ZBOZI:
                return new ZboziDAO();
            case POBOCKA:
                return new PobockaDAO();
            case ZAMESTNANEC:
                return new ZamestnanecDAO();
                default:
                    return null;

        }
    }

    public final static IDataMapper getXmlDAO(DAOType daoType){
        switch(daoType){
            case LOGIN:
                return new LoginDAO();
                default:
                    return null;
        }
    }
}
