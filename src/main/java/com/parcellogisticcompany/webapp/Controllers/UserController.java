package com.parcellogisticcompany.webapp.Controllers;

import com.parcellogisticcompany.webapp.DTO.Adresat;
import com.parcellogisticcompany.webapp.DTO.Login;
import com.parcellogisticcompany.webapp.DTO.Polozka;
import com.parcellogisticcompany.webapp.DTO.Zasilka;
import com.parcellogisticcompany.webapp.Services.AdresatService;
import com.parcellogisticcompany.webapp.Services.LoginService;
import com.parcellogisticcompany.webapp.Services.ZasilkaService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/adresat")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {


    public AdresatService adresatService = new AdresatService();
    public ZasilkaService zasilkaService = new ZasilkaService();
    public LoginService loginService = new LoginService();

    @GetMapping("/{id}")
    public Adresat getAdresat(@PathVariable("id") int id) throws SQLException {
        return this.adresatService.getOneAdresat(id);
    }

    @GetMapping("/{id}/zasilka")
    public List<Zasilka> getAllAdresatZasilkas(@PathVariable("id") int id) throws SQLException {
        return this.zasilkaService.getAllAdresatZasilkas(id);
    }

    @PostMapping("/{id}/zasilka")
    public ResponseEntity addZasilka(@PathVariable("id") int id, @RequestBody Zasilka zasilka){
        Adresat adresat = new Adresat();
        adresat.setAid(id);

        zasilka.setAdresat(adresat);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/login")
    public Login logIn(@RequestBody Login login) throws SQLException {
        System.out.println("REST " + login);

        return this.loginService.getLoginCredentials(login);
    }

}
