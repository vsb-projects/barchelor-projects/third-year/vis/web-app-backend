package com.parcellogisticcompany.webapp.Controllers;

import com.parcellogisticcompany.webapp.DTO.Odesilatel;
import com.parcellogisticcompany.webapp.DTO.Polozka;
import com.parcellogisticcompany.webapp.DTO.Zasilka;
import com.parcellogisticcompany.webapp.DTO.ZasilkaMapper;
import com.parcellogisticcompany.webapp.Services.OdesilatelService;
import com.parcellogisticcompany.webapp.Services.PolozkaService;
import com.parcellogisticcompany.webapp.Services.ZasilkaService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/zasilka")
@CrossOrigin(origins = "http://localhost:3000")
public class ZasilkaController {

    public ZasilkaService zasilkaService = new ZasilkaService();
    public PolozkaService polozkaService = new PolozkaService();
    public OdesilatelService odesilatelService = new OdesilatelService();

    @GetMapping("/{id}/polozka")
    public List<Polozka> getZasilkaPolozkas(@PathVariable("id") int id) throws SQLException {
        return polozkaService.getZasilkaPolozkas(id);
    }

    @GetMapping("/{id}")
    public Zasilka getOneZasilka(@PathVariable("id") int id) throws SQLException {
        return zasilkaService.getZasilkaById(id);
    }

    @GetMapping
    public List<Zasilka> getAllZasilkas() throws SQLException {
        return this.zasilkaService.getZasilkas();
    }

    @GetMapping("/{id}/odesilatel")
    public List<Odesilatel> getZasilkaOdesilatel(@PathVariable("id") int id) throws SQLException {
        return odesilatelService.getAllZasilkaOdesilatels(id);
    }

    @PostMapping
    public ResponseEntity addZasilka(@RequestBody ZasilkaMapper zasilkaMapper) throws SQLException {

        System.out.println(zasilkaMapper.toString());

        if(this.zasilkaService.saveNewZasilka(zasilkaMapper)){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteZasilka(@PathVariable("id") int id) throws SQLException {
        if(this.zasilkaService.deleteZasilkaById(id)){
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.badRequest().build();
        }
    }
}
