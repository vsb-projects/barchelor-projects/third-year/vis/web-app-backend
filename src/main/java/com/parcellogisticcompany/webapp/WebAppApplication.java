package com.parcellogisticcompany.webapp;

import com.parcellogisticcompany.webapp.DAO.Factory.DAOFactory;
import com.parcellogisticcompany.webapp.DAO.Factory.DAOType;
import com.parcellogisticcompany.webapp.DAO.IDataMapper;
import com.parcellogisticcompany.webapp.DTO.Login;
import com.parcellogisticcompany.webapp.config.GenerateXml;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

@SpringBootApplication
public class WebAppApplication {

    public static IDataMapper dataMapper = DAOFactory.getXmlDAO(DAOType.LOGIN);

    public static void main(String[] args) throws SQLException {
        GenerateXml generateXml = new GenerateXml();
//        generateXml.generate();

//        Login login = (Login) dataMapper.getByString("ptacekar").orElse(null);

        SpringApplication.run(WebAppApplication.class, args);
    }
}
