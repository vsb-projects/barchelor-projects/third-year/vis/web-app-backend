package com.parcellogisticcompany.webapp.config;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class RowSelectChangeListener implements ChangeListener {

    Integer row = null;

    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {

        row = (Integer) newValue;

        System.out.println("hmmmm " + row);

    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }
}
