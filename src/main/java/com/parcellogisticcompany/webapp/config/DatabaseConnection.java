package com.parcellogisticcompany.webapp.config;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DatabaseConnection {

    private static final Logger logger = Logger.getLogger(DatabaseConnection.class.getName());
    private static final String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//    private static final String DB_CONNECTION = "jdbc:sqlserver://localhost:1433/visDB";
    private static final String DB_CONNECTION = "jdbc:sqlserver://localhost:1433;DatabaseName=visDB";
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "Heslo111";
    private static final String connectionUrl = "jdbc:jtds:sqlserver://localhost:1433/visDB";

    public DatabaseConnection() {

    }

    public static Connection getDBConnection() throws SQLException {

        String SQL = "SELECT * from adresat";

        Connection conn = null;

        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException exception) {
            logger.log(Level.SEVERE, "Hmmmmm " + exception.getMessage());
        }


        try {

            conn = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            if (conn != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                System.out.println("Driver name: " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product name: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
                System.out.println();
                /*
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(SQL);

                while (rs.next()) {
                    System.out.println(rs.getString("jmeno") + " " + rs.getString("prijmeni"));
                }*/

            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        /*
        finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
*/
//
//        try {
//            Class.forName(DB_DRIVER);
//        } catch (ClassNotFoundException exception) {
//            logger.log(Level.SEVERE, exception.getMessage());
//        }
//
//        try {
//            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
//            return connection;
//        } catch (SQLException exception) {
//            logger.log(Level.SEVERE, exception.getMessage());
//        }

        return conn;
    }

}