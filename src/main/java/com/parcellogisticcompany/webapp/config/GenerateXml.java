package com.parcellogisticcompany.webapp.config;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.parcellogisticcompany.webapp.DTO.Adresat;
import com.parcellogisticcompany.webapp.Services.AdresatService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.File;
import java.sql.SQLException;

public class GenerateXml {
    public GenerateXml() {
    }

    public AdresatService adresatService = new AdresatService();

    public void generate() {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        try {
            dBuilder = dbFactory.newDocumentBuilder();


        Document doc = dBuilder.newDocument();
        //add elements to Document
        Element rootElement = doc.createElement("Users");
        //append root element to document
        doc.appendChild(rootElement);

        for(Adresat adresat : this.adresatService.getAdresats()){
            rootElement.appendChild(getUser(doc,String.valueOf(adresat.getAid()),createLogin(adresat.getJmeno(),adresat.getPrijmeni()),adresat.getJmeno(), adresat.getPrijmeni(),"user"));
        }

        //for output to file, console
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        //for pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);

        //write to console or file
        StreamResult console = new StreamResult(System.out);
        StreamResult file = new StreamResult(new File("loginCredentials.xml"));

        //write data
        transformer.transform(source, console);
        transformer.transform(source, file);
        System.out.println("DONE");

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private Node getUser(Document doc, String id, String login, String name, String surname, String password) {
        Element user = doc.createElement("User");

        //set id attribute
        user.setAttribute("id", id);

        //create login element
        user.appendChild(getUserElements(doc, user, "login", login.toLowerCase()));

        //create name element
        user.appendChild(getUserElements(doc, user, "name", name));

        //create surname element
        user.appendChild(getUserElements(doc, user, "surname", surname));

        //create password element
        user.appendChild(getUserElements(doc, user, "password", password));

        return user;
    }


    //utility method to create text node
    private Node getUserElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    public String createLogin(String name, String surname){
        StringBuilder login = new StringBuilder();

        if(surname.length() < 5){
            int len = 5 - surname.length();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(surname);
            for (int i = 0; i < len; i++){
                stringBuilder.append(i);
            }
            login.append(stringBuilder);
            login.append(name.substring(0,3));
        }
        else{
            login.append(surname.substring(0,5));
            login.append(name.substring(0,3));
        }
        return login.toString().toLowerCase();
    }
}
